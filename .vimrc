" turn on omni completion
filetype plugin on
set omnifunc=syntaxcomplete#Complete

" left column
set nu
set nuw=3 " line numbers column witdh"
highlight LineNr ctermfg=darkGrey

" tab length
set tabstop=4
set softtabstop=0 noexpandtab
set shiftwidth=4"

" always display status line
set laststatus=2

syntax on

" highlight search
set hlsearch
" turn off it with Enter
nnoremap <C-C> :nohlsearch<CR>

" swap files in vim directory
:set directory=$HOME/.vim/swapfiles//
"set noswapfile

" gruvbox theme
colorscheme gruvbox
let g:gruvbox_contrast_dark= "hard"
set background=dark
let g:gruvbox_invert_signs=0

" fix gitgutter background for gruvbox theme
let g:gitgutter_override_sign_column_highlight=1
hi GitGutterAdd          ctermbg=234
hi GitGutterChange       ctermbg=234
hi GitGutterDelete       ctermbg=234
hi GitGutterChangeDelete ctermbg=234

" Ctrl-p searches files with fzf
nnoremap <C-p> :Files<CR>
" Ctrl-g searches text in files with ripgrep
nnoremap <C-g> :Rg<CR>

" navigate vim splits
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" skeleton templates
if has("autocmd")
  augroup templates
    autocmd BufNewFile *.sh 0r ~/.vim/templates/skeleton.sh
    autocmd BufNewFile *.sh normal G
  augroup END
endif"")

" HAProxy syntax
au BufRead,BufNewFile haproxy* set ft=haproxy

set relativenumber

"inoremap " ""<left>
"inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O

imap <C-A> <C-O>0
imap <C-E> <C-O>$

" rename var under cursor
nnoremap <F6> gd:%s//
