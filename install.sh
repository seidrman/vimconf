#!/bin/bash

echo "git pull..."
git pull

echo "updating submodules for vim plugins..."
git submodule update --init --recursive

echo "setting up zsh env for vim theme usage..."
# necessary for gruvbox theme in tmux
if ! grep -q "export TERM=" ${HOME}/.zshenv 2> /dev/null; then
    echo "export TERM=screen-256color" >> ${HOME}/.zshenv
fi
# fix previous export TERM in .zsh
if ! grep -q "DISABLE_AUTO_TITLE" ${HOME}/.zshenv; then
	echo "DISABLE_AUTO_TITLE=true" >> ${HOME}/.zshenv
fi

if [ ! -f ${HOME}/.vimrc ]; then
	echo "linking .vimrc..."
	ln -s ${HOME}/.vim/.vimrc ${HOME}/.vimrc
fi

