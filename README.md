# Vim Configuration

## Instructions
	* make sure you don't have .vim folder and .vimrc file in your home folder 
	* pull this repo in your .vim folder
		git clone git@bitbucket.org:seidrman/vimconf.git ~/.vim
	* run ./install.sh

## Optionally
	* install ripgrep

## Plugins installed with VIM package manager and git submodule
	* FZF, FZF.vim
	* lightline
	* ALE
	* gitgutter
	* NERDTree
    * polyglot
	* gruvbox 
	 * __IMPORTANT__ if used with tmux then add to *.zshrc*:
		```
		export TERM=screen-256color
		```
## Add start plugin with git submodule
```
git submodule add https://github.com/dude/foo.git pack/git-plugins/start/foo
```

## Add optional plugin with git submodule
```
git submodule add https://github.com/dude/foo.git pack/git-plugins/opt/foo
```

enable it with
```
:packadd foo
```

## Notes
	* tested with vim 8.1

